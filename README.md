# APP-VOX

### Desafio VOX
Aplicativo feito para participação do teste para vaga de desenvolvedor pela Vox Tecnologia

### Sobre o aplicativo 
O Aplicativo foi dividido em dois modulos, frontend na pasta app-frontend, feito com Angular 2 usando Bootstrap 4. E backend api rest, usando Laravel 5.4 e autenticação OAuth2. O Banco usado foi o MySQL.

### Instalação
  - Pré-requisitos :
    ```
    * PHP 5.6+
        - Extensão: OpenSSL, PDO, Mbstring, Tokenizer, XML 
    * MySQL
    * Node 
    ```
    
  - ( Processo simples) Instalação usando dump:
    ```
    1. Crie um databse MySQL e importe o dump app-vox.sql
    2. Feito isso, configure o banco de dados no arquivo .env no diretório app-vox.
    3. Após isso só precisa rodar os seguintes comandos:
    *
    * No diretório app-vox:
        - php artisan serve
    *
    * No diretório app-frontend:
        - npm install
        - npm start
    ```
    
  - (Processo complexo) Instalação e configuração do projeto:
    ```
    1. Crie um databse MySQL 
    2. Feito isso, configure o banco de dados no arquivo .env no diretório app-vox.
    3. Após isso só precisa rodar os seguintes comandos:
    *
    * No diretório app-vox:
        - php artisan migrate (para criar as tabelas)
        - php artisan db:seed --class=empresa (Opcional, para criar registros na tabela empresa)
        - php artisan db:seed --class=socio (Opcional, para criar registros na tabela socio)
        - php artisan passport:client --password (Copie e cole em um TXT, vai usar o client secret na próxima etapa)
        - php artisan serve
    *
    4. Acesse o caminho http://127.0.0.1:8000 ou http://localhost:8000 e faça o cadastro de uma conta e faça o login
    5. Utilize o POSTMAN (https://www.getpostman.com/) execute uma requisição POST do tipo form-data com o seguintes valores no body
        - grant_type: password
        - client_id: 2 (geralmente só funciona com o segundo)
        - client_secret: (cole o client secret gerado anteriomente)
        - username: (usuário que cadastrou na API)
        - password: (senha que cadastrou na API)
        - scope: (deixe nulo)
    *
    6. Ele vai gerar o token, cópie e substitua o token dentro de app-frontend/src/app/app-http.service.ts, dentro da função setAccessToken, na variável let token.
    7. Feito isso rode o comandos:
    *
    * No diretório app-frontend:
        - npm install
        - npm start
    ```
    #### Pronto!
    Agora só acessa o link: http://localhost:8080 e validar
    
