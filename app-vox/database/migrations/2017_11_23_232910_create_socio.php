<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('socio', function (Blueprint $table){
            $table->engine = "InnoDB";

            $table->increments("id");
            $table->integer('empresa_id')->unsigned();
            $table->string("nome", 255);
            $table->date("data_aniversaio");
            $table->char("cpf", 14);
            $table->timestamps();
            $table->foreign('empresa_id')->references('id')->on('empresa');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("socio");
    }
}
