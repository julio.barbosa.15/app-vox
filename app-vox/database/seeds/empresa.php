<?php

use Illuminate\Database\Seeder;

class empresa extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empresa')->insert([
            'id' => 1,
            'razao_social' => "Vox Tecnologia Ltda.",
            'nome_fantasia' => "Vox",
            'cnpj' => "52.573.326/0001-01",
        ]);
    }
}
