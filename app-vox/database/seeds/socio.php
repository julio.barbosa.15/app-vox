<?php

use Illuminate\Database\Seeder;

class socio extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            ['empresa_id' => 1, "nome" => "Fernando Gomes de Sousa", "data_aniversaio" => "1985-11-15", "cpf" => "187.762.019-02"],
            ['empresa_id' => 1, "nome" => "Ana Luisa Oliveira", "data_aniversaio" => "1983-01-02", "cpf" => "765.546.254-29"],
            ['empresa_id' => 1, "nome" => "Francisco do Nascimento Pinto", "data_aniversaio" => "1965-09-13", "cpf" => "992.874.666-45"],
            ['empresa_id' => 1, "nome" => "Bruno Chagas", "data_aniversaio" => "1990-12-15", "cpf" => "904.280.571-44"],
            ['empresa_id' => 1, "nome" => "Salete Maria Carvalho de Lima Oliveira", "data_aniversaio" => "1967-10-22", "cpf" => "314.038.011-93"]
        ];

        DB::table('socio')->insert($data);


    }
}
