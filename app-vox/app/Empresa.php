<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'razao_social', 'nome_fantasia', 'cnpj'
    ];

    public function socio()
    {
        return $this->hasMany(Socio::class, 'empresa_id');
    }
}
