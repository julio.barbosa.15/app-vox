<?php

namespace App\Http\Controllers\Api;

use App\Empresa;
use App\Socio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocioController extends Controller
{
    use \App\Http\Controllers\ApiControllerTrait;

    //
    protected $model;
    protected $relatioships = ['empresa'];

    public function __construct(Socio $model)
    {
        $this->model = $model;
    }
}
