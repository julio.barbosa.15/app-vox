<?php

namespace App\Http\Controllers\Api;

use App\Empresa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmpresaController extends Controller
{
    use \App\Http\Controllers\ApiControllerTrait;

    //
    protected $model;
    protected $relatioships = ['socio'];

    public function __construct(Empresa $model)
    {
        $this->model = $model;
    }
}
