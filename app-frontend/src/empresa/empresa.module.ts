import {NgModule} from "@angular/core";
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {AppHttpService} from "../app/app-http.service";

import {EmpresaComponent} from "./empresa.component";
import {EmpresaNewComponent} from "./empresa-new.component";
import {EmpresaViewComponent} from "./empresa-view.component";
import {EmpresaEditComponent} from "./empresa-edit.component";

const appRoutes: Routes = [
    {path: 'empresa', component: EmpresaComponent},
    {path: 'empresa/new', component: EmpresaNewComponent},
    {path: 'empresa/:id', component: EmpresaViewComponent},
    {path: 'empresa/:id/edit', component: EmpresaEditComponent},
];

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        FormsModule,
    ],
    declarations: [
        EmpresaComponent,
        EmpresaNewComponent,
        EmpresaViewComponent,
        EmpresaEditComponent
    ],
    bootstrap: [EmpresaComponent],
    providers: [AppHttpService]
})

export class EmpresaModule {
}