import {Component} from '@angular/core';
import {AppHttpService} from '../app/app-http.service';
import {ActivatedRoute, Params} from '@angular/router';


@Component({
    templateUrl: './empresa-view.component.html',
})

export class EmpresaViewComponent {
    public empresa: Object = {
        socio: [{}]
    };

    constructor(private httpService: AppHttpService,
                private router: ActivatedRoute) {
    }

    ngOnInit() {
        this.router.params.subscribe((params: any) => {
            this.view(params.id)
        });
    }

    view(id: number) {
        this.httpService.builder('empresa')
            .view(id)
            .map((res) => res.json())
            .subscribe(
                data => this.empresa = data,
            );
    }
}