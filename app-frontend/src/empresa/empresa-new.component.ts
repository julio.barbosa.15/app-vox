import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AppHttpService} from '../app/app-http.service';


@Component({
    templateUrl: './empresa-new.component.html',
})

export class EmpresaNewComponent {

    public empresa: Object = {};

    constructor(private httpService: AppHttpService,
                private router: Router) {
    }

    save() {
        this.httpService.builder('empresa')
            .insert(this.empresa)
            .then((res) => {
                this.router.navigate(['/empresa']);
            });
    }

}