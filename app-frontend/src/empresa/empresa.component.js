"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var app_http_service_1 = require("../app/app-http.service");
var EmpresaComponent = (function () {
    function EmpresaComponent(httpService, router) {
        this.httpService = httpService;
        this.router = router;
        this.list();
    }
    EmpresaComponent.prototype.nbOnInit = function () {
        this.list();
    };
    EmpresaComponent.prototype.list = function () {
        var _this = this;
        this.httpService.builder('empresa')
            .list()
            .then(function (res) {
            _this.empresas = res.data;
        });
    };
    EmpresaComponent.prototype.delete = function (id, name) {
        if (confirm("Deseja realmente deletar " + name)) {
            this.httpService.builder('empresa')
                .delete(id)
                .then(function (res) {
                window.location.reload();
            });
        }
    };
    EmpresaComponent = __decorate([
        core_1.Component({
            templateUrl: './empresa.component.html',
            styles: ['a {cursor: pointer}'],
        }),
        __metadata("design:paramtypes", [app_http_service_1.AppHttpService,
            router_1.Router])
    ], EmpresaComponent);
    return EmpresaComponent;
}());
exports.EmpresaComponent = EmpresaComponent;
//# sourceMappingURL=empresa.component.js.map