import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AppHttpService} from "../app/app-http.service";

@Component({
    templateUrl: './empresa.component.html',
    styles: ['a {cursor: pointer}'],
})

export class EmpresaComponent {

    public empresas: Array<Object>;

    constructor(private httpService: AppHttpService,
                private router: Router) {
        this.list();
    }

    nbOnInit(){
        this.list();
    }

    list() {
        this.httpService.builder('empresa')
            .list()
            .then((res) => {
                this.empresas = res.data
            })
    }

    delete(id: number, name: string) {
        if(confirm("Deseja realmente deletar "+name)) {
            this.httpService.builder('empresa')
                .delete(id)
                .then((res)=>{
                    window.location.reload();
                });
        }

    }

}