import {Component} from '@angular/core';
import {AppHttpService} from '../app/app-http.service';
import {Router, ActivatedRoute, Params} from '@angular/router';


@Component({
    templateUrl: './empresa-edit.component.html',
})

export class EmpresaEditComponent {
    public empresa: Object = {};

    constructor(
        private httpService: AppHttpService,
        private router: ActivatedRoute,
        private route: Router
    ) {}

    ngOnInit() {
        this.router.params.subscribe((params:any) => {
            this.view(params.id)
        });
    }

    view(id: number) {
        this.httpService.builder('empresa')
            .view(id)
            .map((res) => res.json())
            .subscribe(
                data => this.empresa = data,
            );
    }

    save(id: number){
        this.httpService.builder('empresa')
            .update(id, this.empresa)
            .then((res) => {
                this.route.navigate(['/empresa']);
            });
    }
}