"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var app_http_service_1 = require("../app/app-http.service");
var empresa_component_1 = require("./empresa.component");
var empresa_new_component_1 = require("./empresa-new.component");
var empresa_view_component_1 = require("./empresa-view.component");
var empresa_edit_component_1 = require("./empresa-edit.component");
var appRoutes = [
    { path: 'empresa', component: empresa_component_1.EmpresaComponent },
    { path: 'empresa/new', component: empresa_new_component_1.EmpresaNewComponent },
    { path: 'empresa/:id', component: empresa_view_component_1.EmpresaViewComponent },
    { path: 'empresa/:id/edit', component: empresa_edit_component_1.EmpresaEditComponent },
];
var EmpresaModule = (function () {
    function EmpresaModule() {
    }
    EmpresaModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                router_1.RouterModule.forRoot(appRoutes),
                forms_1.FormsModule,
            ],
            declarations: [
                empresa_component_1.EmpresaComponent,
                empresa_new_component_1.EmpresaNewComponent,
                empresa_view_component_1.EmpresaViewComponent,
                empresa_edit_component_1.EmpresaEditComponent
            ],
            bootstrap: [empresa_component_1.EmpresaComponent],
            providers: [app_http_service_1.AppHttpService]
        })
    ], EmpresaModule);
    return EmpresaModule;
}());
exports.EmpresaModule = EmpresaModule;
//# sourceMappingURL=empresa.module.js.map