import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from "@angular/http";

import {AppComponent} from './app.component';

import {HomeModule} from "../home/home.module";
import {EmpresaModule} from "../empresa/empresa.module";
import {SocioModule} from "../socio/socio.module";

const appRoutes: Routes = [
    {path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
    imports: [
        BrowserModule,
        HomeModule,
        EmpresaModule,
        SocioModule,
        RouterModule.forRoot(appRoutes),
        HttpModule,
    ],
    declarations: [
        AppComponent
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}