/**
 * Created by julio on 09/06/17.
 */
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()

export class AppHttpService {
    private url: string;
    private header: Headers;

    constructor (private http: Http){
        this.setAccessToken();
    }

    setAccessToken(){
        let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU0ZjNjZTBjMTI3ZjRkNzA3ODAyZDY0YmI3MDNjZWMzOWRmNmQxMjIxNTM3NzgwNmJhOTY5Yjk3ZGFmYThkN2U2YTU4ZDU2ZDQ2MTVmMDNjIn0.eyJhdWQiOiIyIiwianRpIjoiNTRmM2NlMGMxMjdmNGQ3MDc4MDJkNjRiYjcwM2NlYzM5ZGY2ZDEyMjE1Mzc3ODA2YmE5NjliOTdkYWZhOGQ3ZTZhNThkNTZkNDYxNWYwM2MiLCJpYXQiOjE1MTE0OTM0NTcsIm5iZiI6MTUxMTQ5MzQ1NywiZXhwIjoxNTQzMDI5NDU3LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.J4SsQ2BnMiFFl-3t2omx1Gg6GGkYh99Dx1RE01V157lewyvqu3q4AICycS_A4r_AOB31Lba88L7MfyDaHuHP3ydIvWcg2q40n-rtoXwJQB0qcNoD7J3a0CuyMeyMjfMtR4KbR4f4kwBZnHmHWJk9ZgypUry7dsNGn2zWryMVgeQz1D2tTwbaQ7XMBxO_1Si5fS5bvGXZuevZPAj2Avo_kQnaHIS0U_RNK4HR3fPIPCXpz0OgLaQv9fmANcIoi_XEzaXdCNliZ3_mXXZMu1WvZNqdFxcEz34R44Gk4rIUnRwWgIk2iJT9tR6bBBfQX8pgln5onQ4nBJtfAkljxsAICBL4pb9HVdMDXqvIu8dkn2LAqnMfMXy4JECcfLGGDTbf9TnaKLIFrWiZdS6rksQ6F3UxildJhRxb-ZQ1Nl96KCAg5BtIiHgOBOw38-PV3ID5I_YTLtWNQYkrcIRqsVJYWZn1aQrVUeEFwaRBqfUzNGG10wlttzmISod9-bXh9R6XaV-qKbV6wtkAjdN39vpLMQ9vo3GkQYWWOT28PxHGrqgLBV5eayYS37mGuo9Wy2zehd2OJT6numvj_pnUmA4E0VviGbymk_U-hFFBFM8hzEi55-oRizMFy1jRdWHmUO0JFuQLBMdlB7hl8zzlKAP4h6kg3fS5SlSDO16HkqqvMg8';
        this.header = new Headers({
            'Authorization' : 'Bearer ' + token
        });
    }

    builder(resource: string){
        this.url = 'http://localhost:8000/api/' + resource;
        return this;
    }

    list(){
        return this.http.get(this.url, {headers: this.header})
            .toPromise()
            .then((res) => {
                return res.json() || {};
            });
    }

    view(id: number){
        return this.http.get(this.url + '/' + id, {headers: this.header});
    }

    update(id: number, data: Object){
        return this.http.put(this.url + '/' + id, data, {headers: this.header})
            .toPromise()
            .then((res) => {
                return res.json() || {};
            });
    }

    insert(data: Object){
        return this.http.post(this.url, data, {headers: this.header})
            .toPromise()
            .then((res) => {
                return res.json() || {};
            });
    }

    delete(id: number){
        return this.http.delete(this.url + '/' + id, {headers: this.header})
            .toPromise()
            .then((res)=>{
                return res.json() || {};
            });
    }
}