import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AppHttpService} from '../app/app-http.service';


@Component({
    templateUrl: './socio-new.component.html',
})

export class SocioNewComponent {

    public socio: Object = {};
    public empresa: Array<Object>;

    constructor(private httpService: AppHttpService,
                private router: Router) {
    }

    ngOnInit() {
        this.empresaList();
    }

    empresaList() {
        this.httpService.builder('empresa')
            .list()
            .then((res) => {
                this.empresa = res.data;
            })
    }

    save() {
        this.httpService.builder('socio')
            .insert(this.socio)
            .then((res) => {
                this.router.navigate(['/socio']);
            });
    }

}