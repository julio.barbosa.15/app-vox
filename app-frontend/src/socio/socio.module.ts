import {NgModule} from "@angular/core";
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {AppHttpService} from "../app/app-http.service";

import {SocioComponent} from "./socio.component";
import {SocioNewComponent} from "./socio-new.component";
import {SocioEditComponent} from "./socio-edit.component";

const appRoutes: Routes = [
    {path: 'socio', component: SocioComponent},
    {path: 'socio/new', component: SocioNewComponent},
    {path: 'socio/:id/edit', component: SocioEditComponent},
];

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        FormsModule,
    ],
    declarations: [
        SocioComponent,
        SocioNewComponent,
        SocioEditComponent
    ],
    bootstrap: [SocioComponent],
    providers: [AppHttpService]
})

export class SocioModule {
}