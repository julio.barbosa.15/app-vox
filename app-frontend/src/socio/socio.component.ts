import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AppHttpService} from "../app/app-http.service";

@Component({
    templateUrl: './socio.component.html',
    styles: ['a {cursor: pointer}'],
})

export class SocioComponent {
    public socios: Object = [{
        empresa: {}
    }];

    constructor(private httpService: AppHttpService,
                private router: Router) {
        this.list();
    }

    nbOnInit() {
        this.list();
    }

    list() {
        this.httpService.builder('socio')
            .list()
            .then((res) => {
                this.socios = res.data
            })
    }

    delete(id: number, name: string) {
        if (confirm("Deseja realmente deletar " + name)) {
            this.httpService.builder('socio')
                .delete(id)
                .then((res) => {
                    window.location.reload();
                });
        }

    }

}