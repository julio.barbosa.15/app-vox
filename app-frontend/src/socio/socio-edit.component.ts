import {Component} from '@angular/core';
import {AppHttpService} from '../app/app-http.service';
import {Router, ActivatedRoute, Params} from '@angular/router';


@Component({
    templateUrl: './socio-edit.component.html',
})

export class SocioEditComponent {
    public socio: Object = {
        empresa: {}
    };

    public empresas: Array<Object>;

    constructor(
        private httpService: AppHttpService,
        private router: ActivatedRoute,
        private route: Router
    ) {}

    ngOnInit() {
        this.router.params.subscribe((params:any) => {
            this.view(params.id)
        });
        this.empresaList();
    }

    empresaList() {
        this.httpService.builder('empresa')
            .list()
            .then((res) => {
                this.empresas = res.data;
            })
    }

    view(id: number) {
        this.httpService.builder('socio')
            .view(id)
            .map((res) => res.json())
            .subscribe(
                data => this.socio = data,
            );
    }

    save(id: number){
        this.httpService.builder('socio')
            .update(id, this.socio)
            .then((res) => {
                this.route.navigate(['/socio']);
            });
    }
}