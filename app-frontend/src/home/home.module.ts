import {NgModule} from "@angular/core";
import {BrowserModule}  from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home.component";

const appRoutes: Routes = [
    {path: 'home', component: HomeComponent},
];

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
    ],
    declarations: [
        HomeComponent
    ],
    bootstrap: [HomeComponent],
})

export class HomeModule {
}